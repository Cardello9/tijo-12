package com.demo.springboot.domain.dto;

import java.io.Serializable;

public class MovieDto implements Serializable {

    // TODO: Pola klasy powinny miec identyczne nazwy jak klucze w JSONie
    // TODO: Bardzo prosze dokonczyc implementacje klasy

    private String movieId, title, year, image;

    public MovieDto(String movieId, String title, String year, String image) {
        this.movieId = movieId;
        this.title = title;
        this.year = year;
        this.image = image;
    }

    public String getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getImage() {
        return image;
    }
}
