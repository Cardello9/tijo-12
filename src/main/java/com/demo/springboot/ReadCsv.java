package com.demo.springboot;

import com.demo.springboot.domain.dto.MovieDto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadCsv {
    List<MovieDto> movies = new ArrayList<>();
    boolean skipRecordFlag = false;

    String line;
    BufferedReader br;

    public List<MovieDto> myRead() {
            try {
                br = new BufferedReader(new FileReader("./src/main/resources/movies.csv"));
                while ((line = br.readLine()) != null) {
                    String[] values = line.split(";");
                    MovieDto movieDto = new MovieDto(values[0], values[1], values[2], values[3]);
                    if (skipRecordFlag == true) {
                        movies.add(movieDto);
                    }
                    skipRecordFlag = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        return movies;
    }

}
